﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp64
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            bt1.BackColor = Color.LightPink;
            bt1.Parent = this;
            bt1.Location = new Point(150, 50);
            bt1.Text = "execute";
            bt1.MouseMove += Bt1_MouseMove;
            bt1.MouseLeave += Bt1_MouseLeave;
            bt1.FlatStyle = FlatStyle.Flat;
            bt1.AutoSize=true;
            bt1.Click += Bt1_Click;
            bt2.BackColor = Color.LightPink;
            bt2.Location = new Point(230, 200);
            bt2.Parent = this;
            bt2.Text = "back";
            bt2.FlatStyle = FlatStyle.Flat;
            bt2.MouseLeave += Bt2_MouseLeave;
            bt2.AutoSize = true;
            bt2.MouseMove += Bt2_MouseMove;
            bt2.Click += Bt2_Click;
            text.BorderStyle = BorderStyle.Fixed3D;
            text.Location = new Point(170,70);
            text.Parent = this;
            text.AutoSize = true;
            bt3.BackColor = Color.LightPink;
            bt3.Parent = this;
            bt3.AutoSize = true;
            bt3.Location = new Point(120, 200);
            bt3.Text = "exit";
            bt3.MouseMove += Bt3_MouseMove;
            bt3.MouseLeave += Bt3_MouseLeave;
            bt3.Click += Bt3_Click;
            bt3.FlatStyle = FlatStyle.Flat;
        }

        private void Bt3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Bt3_MouseLeave(object sender, EventArgs e)
        {
            bt3.BackColor = Color.Blue;
            bt3.ForeColor = Color.Black;
        }

        private void Bt3_MouseMove(object sender, MouseEventArgs e)
        {
            bt3.BackColor = Color.Blue;
            bt3.ForeColor = Color.Black;
        }

        private void Bt2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void Bt2_MouseMove(object sender, MouseEventArgs e)
        {
            bt2.BackColor = Color.Blue;
            bt2.ForeColor = Color.Black;
        }

        private void Bt2_MouseLeave(object sender, EventArgs e)
        {
            bt2.BackColor = Color.Blue;
            bt2.ForeColor = Color.Black;
        }

        private void Bt1_Click(object sender, EventArgs e)
        {
            text.Text = "end!";
        }

        private void Bt1_MouseLeave(object sender, EventArgs e)
        {
            bt1.BackColor = Color.LightGreen;
            bt1.ForeColor = Color.Black;
        }

        private void Bt1_MouseMove(object sender, MouseEventArgs e)
        {
            bt1.BackColor = Color.Blue;
            bt1.ForeColor = Color.Black;
        }
        Label text = new Label();
        Button bt1 = new Button();
        Button bt2 = new Button();
        Button bt3 = new Button();
    }

}
